/* eslint-disable no-console */
import { AirtableRecordsResponse, VacantSpace } from "~/types"

const AIRTABLE_BASE_ID = process.env.NUXT_ENV_AIRTABLE_BASE_ID
const AIRTABLE_TABLE_NAME = process.env.NUXT_ENV_AIRTABLE_TABLE_NAME
const AIRTABLE_ACCESS_TOKEN = process.env.NUXT_ENV_ACCESS_TOKEN

export default async (offset?: string): Promise<AirtableRecordsResponse<VacantSpace>> => {
  const sortParams = `&sort%5B0%5D%5Bfield%5D=created&sort%5B0%5D%5Bdirection%5D=asc`
  const offsetParams = offset ? `&offset=${offset}` : ''

  const resp = await fetch(
    `https://api.airtable.com/v0/${AIRTABLE_BASE_ID}/${AIRTABLE_TABLE_NAME}?${sortParams}${offsetParams}`,
    {
      headers: new Headers({
        'Authorization': `Bearer ${AIRTABLE_ACCESS_TOKEN}` 
      })
    }
  )

  const res: AirtableRecordsResponse<VacantSpace> = await resp.json()

  if (res.error) {
    console.error(JSON.stringify(res.error))
    throw new Error(JSON.stringify(res.error))
  }

  return res

}
