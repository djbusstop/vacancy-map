/* eslint-disable no-console */
import extractFields from "./extractFields"
import { AirtableRecordsResponse, VacantSpace } from "~/types"

const AIRTABLE_BASE_ID = process.env.NUXT_ENV_AIRTABLE_BASE_ID
const AIRTABLE_TABLE_NAME = process.env.NUXT_ENV_AIRTABLE_TABLE_NAME
const AIRTABLE_ACCESS_TOKEN = process.env.NUXT_ENV_ACCESS_TOKEN

export default async (vacantSpace: VacantSpace): Promise<unknown> => {
  const body = {
    records: [
      {
        fields: vacantSpace
      }
    ],
  }

  const resp = await fetch(
    `https://api.airtable.com/v0/${AIRTABLE_BASE_ID}/${AIRTABLE_TABLE_NAME}`,
    {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${AIRTABLE_ACCESS_TOKEN}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    }
  )

  const { error, records }: AirtableRecordsResponse<VacantSpace> = await resp.json()

  if (records) {
    const resSpaces = extractFields(records)
    if (resSpaces.length > 0) return resSpaces[0];
    console.warn('For some reason the response did not contain the new record')
    return undefined
  }

  if (error) {
    throw new Error(error as string)
  }

}
