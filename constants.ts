import { VACANT_SPACE_OWNER, VACANT_SPACE_STATUS } from "./types"

export const OWNER_OPTIONS = [
  {
    text: 'Public - Federal',
    value:  VACANT_SPACE_OWNER.PUBLIC_FEDERAL,
    color: "#ef3340"
  },
  {
    text: 'Public - Provincial',
    value:   VACANT_SPACE_OWNER.PUBLIC_PROVINCIAL,
    color: "#3d64f3"
  },
  {
    text: 'Public - Municipal',
    value: VACANT_SPACE_OWNER.PUBLIC_MUNICIPAL,
    color: "#F36A3D"
  },
  {
    text: 'Private',
    value: VACANT_SPACE_OWNER.PRIVATE,
    color: "#333"
  },
  {
    text: "Unknown",
    value: VACANT_SPACE_OWNER.UNKNOWN,
    color: "#fff"
  },
]

type OwnerOptionsConfig = {[key in VACANT_SPACE_OWNER]: {text: string; value: string; color: string; }}
export const ownerOptionsConfig = OWNER_OPTIONS.reduce((acc, option) => {
  return {
    [option.value]: option,
    ...acc
  }
}, {}) as OwnerOptionsConfig

export const STATUS_OPTIONS = [
  {
    text: "Vacant",
    value: VACANT_SPACE_STATUS.VACANT
  },
  {
    text: "Squatted",
    value: VACANT_SPACE_STATUS.SQUATTED
  },
  {
    text: "In use",
    value:  VACANT_SPACE_STATUS.IN_USE
  }
]
